import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CommonPage {

    public CommonPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public final String LEFT_SIDE_MENU_ITEM = "//*[@class='level-top'][contains(.,'%s')]";
    public final String MENU_ITEM_ID = "//*[@id='%s']";
    public final String PURCHASE_BUCKET = "//*[@id='topCartTitle'][contains(.,'%s')]";

    @FindBy(css = ".modal-submit-button")
    private WebElement submit_button;

    @FindBy(css = ".close-prompt")
    private WebElement close_notification_popup;

    @FindBy(css = "#search")
    private WebElement main_search_input;

    @FindBy(css = "button")
    private WebElement button;

    @FindBy(css = "#item-1")
    private WebElement first_element;

    @FindBy(css = "#item-1 .buy.small")
    private WebElement buy_button;

    @FindBy(css = ".v-modal__cmp")
    private WebElement order_popup;

    @FindBy(css = ".order-now")
    private WebElement order_button;

    @FindBy(css = ".onepage_wrapper")
    private WebElement order_table;

    @FindBy(css = ".v_onepage_logo.d_logo")
    private WebElement logo;

    //    GETTERS

    public WebElement getSubmit_button() {
        return submit_button;
    }

    public WebElement getClose_notification_popup() {
        return close_notification_popup;
    }

    public WebElement getMain_search_input() {
        return main_search_input;
    }

    public WebElement getButton() {
        return button;
    }

    public WebElement getFirst_element() {
        return first_element;
    }

    public WebElement getBuy_button() {
        return buy_button;
    }

    public WebElement getOrder_popup() {
        return order_popup;
    }

    public WebElement getOrder_button() {
        return order_button;
    }

    public WebElement getOrder_table() {
        return order_table;
    }

    public WebElement getLogo() {
        return logo;
    }
}
