import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends CommonPage {

   public HomePage(WebDriver driver) {
       super(driver);
       PageFactory.initElements(driver, this);

   }

   public final String LOCATION_CITY = "//*[@class='city-name'][contains(.,'%s')]";
   public final String ITEM_CITY = "//*[@class='main-cities-item '][contains(.,'%s')]";



   @FindBy(css = "#account-top-name")
    private WebElement accountButton;

    @FindBy(css = "#customer-popup-menu")
    private WebElement loginPopup;

    @FindBy(css = ".email-login")
    private WebElement email_input;

    @FindBy(css = "#v-login-password")
    private WebElement password_input;

    @FindBy(xpath = "//div[@id='account-top-name'][contains(.,'Oleg')]")
    private WebElement account_name;

    @FindBy(css = ".city-confirmation-tooltip")
    private WebElement city_tooltip;

    @FindBy(css = ".confirm-city")
    private WebElement confirm_city_tooltip_button;

    @FindBy(css = ".city-select-block-menu .city-name")
    private WebElement city_button;

    @FindBy(css = ".city-select-popup")
    private WebElement city_list_popup;

//    GETTERS

    public WebElement getAccountButton() {
        return accountButton;
    }

    public WebElement getLoginPopup() {
        return loginPopup;
    }

    public WebElement getEmail_input() {
        return email_input;
    }

    public WebElement getPassword_input() {
        return password_input;
    }

    public WebElement getAccount_name() {
        return account_name;
    }

    public WebElement getCity_tooltip() {
        return city_tooltip;
    }

    public WebElement getConfirm_city_tooltip_button() {
        return confirm_city_tooltip_button;
    }

    public WebElement getCity_button() {
        return city_button;
    }

    public WebElement getCity_list_popup() {
        return city_list_popup;
    }
}
