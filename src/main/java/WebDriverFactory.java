import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverFactory {

    public static WebDriver getDriver(String name){
        WebDriver driver = null;
        if (name.equals("chrome")){
            System.setProperty("webdriver.chrome.driver","src/main/resources/drivers/chromedriver");
            driver = new ChromeDriver();
        }
        else if(name.equals("ff")){
            System.setProperty("webdriver.gecko.driver","src/main/resources/drivers/geckodriver");
            driver=new FirefoxDriver();
        }
        return driver;
    }

}
